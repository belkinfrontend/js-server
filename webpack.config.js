const path = require('path');

module.exports = {
    mode: 'development',

    entry: './source/js/main.js',
    // target: 'web',
    output: {
        filename: 'bundle.js',
        //path: path.resolve(__dirname, './source/js')
    },
    module: {
        rules: [{
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.pug$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'pug-loader'
                }
            },
            {
                test: /\.svg$/,
                use: {
                    loader: 'svg-inline-loader'
                }
            }
        ]
    },
    devtool: 'eval-source-map'
};