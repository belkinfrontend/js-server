export function createHeader() {
    return `<div class="header">
                <div class="person">
                    <div class="avatar"></div>
                    <p class="name">Ivan Abramov's space - boards</p>
                </div>
                <div class="links">
                    <span id="board">board</span>
                    <span id="calendar">calendar</span>
                </div>
            </div>`;
}