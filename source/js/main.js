import { initRouter, navigateTo } from './router-browser';

import { boardView } from './views/BoardView';
import { calendarView } from './views/CalendarView';

import Sortable from 'sortablejs';

import { createHeader } from './createHeader';
import { createFooter } from './createFooter';

//=========================== Icons in SVG format
import plusSVG from '../images/plus.svg';
import rotateSVG from '../images/rotate.svg';
import removeSVG from '../images/remove.svg';
import completeSVG from '../images/complete.svg';


const postData = [];

console.log(postData);

//==================== grab id="container"
const appContainer = document.querySelector('#container');

appContainer.innerHTML = `${createHeader()}
                        <div id="portal"></div>
                        ${createFooter()}
                    `;


function renderTo(targetEl, content) {
    targetEl.innerHTML = content;
}

const views = [{
    content: boardView(postData),
}, {
    content: calendarView()
}];

const portal = document.getElementById('portal');

views.forEach(({ content }) => {
    portal.innerHTML += content;
});

initRouter([{
        route: 'board',
        isDefault: true,
        handler: () => {
            Array.from(portal.childNodes).forEach((element) => { element.style = 'display: none' });
            document.querySelector('.board').style = 'display: flex';
        }
    },
    {
        route: 'calendar',
        isDefault: false,
        handler: () => {
            Array.from(portal.childNodes).forEach((element) => { element.style = 'display: none' });
            document.querySelector('.calendar').style = 'display: flex';
        }
    }
]);

document.getElementById('board').addEventListener('click', () => { navigateTo('#board'); });
document.getElementById('calendar').addEventListener('click', () => { navigateTo('#calendar'); });





// User clicked on the add button
// If there is any text inside the item field, add that text to the todo list

const buttonsArr = document.getElementsByClassName('add');
const buttons = Array.from(buttonsArr);

console.log(buttons);

buttons.map((button) => button.addEventListener('click', function() {
    let value = document.querySelector('.item').value;
    if (value) {
        addItemTodo(value);
        document.querySelector('.item').value = '';
    }

    //alert("button");

}));




// document.querySelector('.add').addEventListener('click', function() {
//     let value = document.querySelector('.item').value;
//     if (value) {
//         addItemTodo(value);
//         document.querySelector('.item').value = '';
//     }
// });

const addItem = () => {
    let value = document.querySelector('.item').value;
    if (value) {
        addItemTodo(value);
        document.querySelector('.item').value = '';
    }
}

document.querySelector('.item').addEventListener('keydown', function(event) {
    let value = event.currentTarget.value;

    if ((event.code === 'Enter' || event.code === 'NumpadEnter') && value) {
        addItemTodo(value);
        document.querySelector('.item').value = '';
    }
});


const changeItem = (event) => {
    let item = event.currentTarget.parentNode.parentNode.childNodes[0];
    let itemChange = event.currentTarget;
    console.log(item);

    if (item.disabled == true) {
        item.removeAttribute('disabled');
        item.style.cursor = 'text';
        item.style.border = '1px solid #25b99a';
        item.focus();

    } else {
        item.setAttribute('disabled', true);
        item.style.cursor = 'move';
        item.style.border = '1px solid #838485';
    }
}

const removeItem = (event) => {
    let item = event.currentTarget.parentNode.parentNode;
    let parent = item.parentNode;

    parent.removeChild(item);
}

const completeItem = (event) => {
    let item = event.currentTarget.parentNode.parentNode;
    let parent = item.parentNode;
    let id = parent.id;
    let value = item.innerText;

    //===== Check if the item should be added to the completed list or to re-added to the todo list
    let target = (id === 'todo') ? document.getElementById('completed') : document.getElementById('todo');

    parent.removeChild(item);
    target.insertBefore(item, target.childNodes[0]);
}

//===== Adds a new item to the to do list

const addItemTodo = (text) => {
    let list = document.getElementById('todo');
    let item = document.createElement('li');
    item.setAttribute("draggable", true);

    let itemInput = document.createElement('input');
    itemInput.setAttribute("disabled", "true");
    itemInput.style.cursor = 'move';
    itemInput.style.border = '1px solid #838485';
    itemInput.value = text;

    let buttons = document.createElement('div');
    buttons.classList.add('buttons');

    let change = document.createElement('button');
    change.classList.add('change');
    change.innerHTML = rotateSVG;

    let remove = document.createElement('button');
    remove.classList.add('remove');
    remove.innerHTML = removeSVG;

    let complete = document.createElement('button');
    complete.classList.add('complete');
    complete.innerHTML = completeSVG;

    //===== Add click event for changing & removing & compliting items
    change.addEventListener('click', changeItem);
    remove.addEventListener('click', removeItem);
    complete.addEventListener('click', completeItem);

    //===== appendChild

    buttons.appendChild(change);
    buttons.appendChild(remove);
    buttons.appendChild(complete);
    item.appendChild(itemInput);
    item.appendChild(buttons);
    list.insertBefore(item, list.childNodes[0]);
}





//========================== sortablejs

Sortable.create(todo, {
    animation: 200,
    group: {
        name: "shared",
        //pull: "clone",
        revertClone: true,
    },
    sort: true
});

Sortable.create(completed, {
    group: "shared",
    sort: false
});
//=========================