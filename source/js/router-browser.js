const checkRoute = (routes) => {
    console.log(routes);
    const hash = window.location.hash.split('#')[1];
    const [baseHash, paramHash] = hash.split('/');

    for (let i = 0; i < routes.length; i++) {
        const [baseRoute, param] = routes[i].route.split('/:');
        if (baseRoute.includes(baseHash)) {
            if (param) {
                routes[i].handler && routes[i].handler(paramHash);
            } else {
                routes[i].handler && routes[i].handler();
            }
            break;
        }
    }
}

export function initRouter(routes) {
    const defaultRouteObj = routes.filter((route) => route.isDefault)[0];
    const defaultRoute = defaultRouteObj.route.split('/')[0];

    history.replaceState({}, '', `#${defaultRoute}`);
    defaultRouteObj.handler && defaultRouteObj.handler();

    window.onpopstate = (event) => { checkRoute(routes); };
}

export function navigateTo(url) {
    window.location.href = url;
    history.pushState({ page: url }, null, url);
}