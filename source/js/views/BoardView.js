import plusSVG from '../../images/plus.svg';


const postData = [{
        id: 1,
        name: 'To Do',
        isDoneColumn: false,
        items: [{
                id: '0001',
                text: 'item',
                creationDate: new Date(),
                dueDate: null
            },
            {
                id: '0002',
                text: 'item',
                creationDate: new Date(),
                dueDate: null
            },
            {
                id: '0003',
                text: 'item',
                creationDate: new Date(),
                dueDate: null
            }
        ]
    },
    {
        id: 2,
        name: 'To Review',
        isDoneColumn: false,
        items: [{
            id: '0004',
            text: 'item',
            creationDate: new Date(),
            dueDate: null
        }]
    },
    {
        id: 3,
        name: 'Done',
        isDoneColumn: true,
        items: [{
                id: '0005',
                text: 'item',
                creationDate: new Date(),
                dueDate: null
            },
            {
                id: '0006',
                text: 'item',
                creationDate: new Date(),
                dueDate: null
            }
        ]
    }
];

const createItem = (itemData) => {

    return `<li>
                <small>${itemData.id}</small>
                <h3>${itemData.text}</h3>
                <small>${itemData.creationDate.toLocaleString()}</small>
             </li>`;
}


const createColumn = (columnData) => {


    return `<section class="column-${columnData.id}" > 
                <header>
                    <span>${columnData.id}. ${columnData.name}</span>
                    <input class="item" type="text" placeholder="add new task">
                    <button class="add-${columnData.id}">
                        ${plusSVG}
                    </button>
                </header>
                <div class="container">
                    <ul class="todo getItem" id="todo">
                        ${columnData.items.map((item) => createItem(item)).join("")}
                    </ul>
                </div>
            </section>
            `;

}

export const boardView = () => {
    document.querySelector('body')
        .addEventListener('click', (event) => {
            const { target } = event;
            if (target.classList.contains(`${target.className}`) || !!target.closest('.add')) {
                console.log(`clicked on an add btn ${target.className}`);
                console.log(event);
            }
        });


    return `<main class="board" style="display: none">
                ${postData.map((column) => createColumn(column)).join("")}
                <section class="new-section"><span>+</span>
                </section>
            </main>`;
}