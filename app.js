const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const keys = require('./keys');
const postRouter = require('./routes/post');

const port = process.env.PORT || 8080;
const publicPath = path.join(__dirname, 'public');

const app = express();
app.use(express.static(publicPath));
app.use('/api/post', postRouter);

app.listen(port, () => {
    console.log(`Server has been started on port ${port}`)
});

//===connect to Database
mongoose.connect(keys.mongoURI)
    .then(() => console.log('MongoDB connected'))
    .catch(err => console.error(err))